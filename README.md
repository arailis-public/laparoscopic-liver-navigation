# ARAILIS Liver Navigation

## Description
ARAILIS (Augmented Reality and Artificial Intelligence supported Laparoscopic Imagery in Surgery) is a project that "aims to make Augmented Reality (AR) and Artificial Intelligence (AI) usable for surgical interventions in order to improve minimally invasive surgery by combining pre- and intraoperative data". The system developed by the project is an Image Guidance Navigation System (IGS), which, in more ways than one, aim to bring IGS closer to translation into the operating room where they can assist surgeons in locating target structures of the liver during surgery. Firstly, this is attempted by making the calibration of the laparoscope camera faster and more reliable with a 3D calibration field, and more user-friendly with an application that visually guides the process. Secondly, the prototype IGS developed does away with the dependance on optical tracking systems for the purpose of tracking the laparoscope or registration of a preoperative liver model, alleviating their associated setup time and complicated usage. This is achieved through the use of a SLAM method for tracking the pose of the laparoscope camera, a point-cloud-based 3D reconstruction system which additionally uses disparity estimation and liver segmentation, a region-based ICP for rigid registration, and lastly a CNN-based non-rigid registration method. 

For the time being, we release the following modules belonging to the pipeline:
- Point-cloud-based 3D Reconstruction: https://gitlab.com/arailis-public/mediassist3_densemap
- Region-based ICP Registration: https://gitlab.com/arailis-public/mediassist3_region_register

<!-- [Registration Interface](/Registration_full.png "Registraion Interface") -->

## Pipeline Overview
![Pipeline Overview](/files/Pipeline_overview.png "Pipeline Overview")

## Demo Video
![Demo Video](/files/LaparoscopicLiverNavigation.mp4 "Demo Video")

## Authors and Acknowledgments
The project is a collaboration between groups from a number of academic institutions in the city of Dresden, Germany: Reuben Docea, Micha Pfeiffer, Jan Müller, Katja Krug, Matthias Hardner, Paul Riedel, Martin Menzel, Fiona R. Kolbinger, Laura Frohneberg, Jürgen Weitz and Stefanie Speidel.

![Logos](/files/Logos.png "Logos")
